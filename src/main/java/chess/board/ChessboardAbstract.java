package chess.board;

public abstract class ChessboardAbstract {
    protected int squareSideSize;

    public void setSquareSideSize(int squareSideSize) {
        this.squareSideSize = squareSideSize;
    }

    public int getDimension() {
        return squareSideSize * squareSideSize;
    }

    public static int getFile(String coordinate) {
        char file = coordinate.toLowerCase().charAt(0);
        return file - 97;
    }

    public static int getRrank(String coordinate) {
        char rank = coordinate.toLowerCase().charAt(1);
        return rank - 49;
    }

    public static String getStrCoordinate(int coordinate, int base) {
        var coordinateStr = coordinate < base ? "0" + Integer.toUnsignedString(coordinate, base)
                : Integer.toUnsignedString(coordinate, base);
        var file = (char) (Integer.parseInt(coordinateStr.charAt(1) + "") + 97);
        var rank = Integer.parseInt(coordinateStr.charAt(0) + "") + 1;
        return file + "" + rank;
    }


    public static int getIntToBaseSide(int file, int rank, int base) {
        return Integer.parseInt(rank + "" + file, base);
    }


    public static int getIntFromStrBaseSide(String coordinate, int base) {
        return getIntToBaseSide(getFile(coordinate), getRrank(coordinate), base);
    }
}
