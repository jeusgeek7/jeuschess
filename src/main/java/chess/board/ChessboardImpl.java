package chess.board;

import chess.board.move.DirectionType;
import chess.board.move.MoveType;
import chess.piece.Chessman;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.HashMap;
import java.util.Map;

public class ChessboardImpl extends ChessboardAbstract implements Chessboard {
    static Logger log = LogManager.getLogger();
    private Map<Square, Chessman> pieces = new HashMap<>();


    public Chessman getPiece(String coordination) throws Exception {
        Square square = checkCoordinate(coordination);
        return pieces.get(square);
    }

    @Override
    public void move(String from, String to) {
        try {
            var squareFrom = checkCoordinate(from);
            var squareTo = checkCoordinate(to);
            Chessman chessman = pieces.get(squareFrom);
            pieces.remove(squareFrom);
            pieces.put(squareTo, chessman);
        } catch (Exception e) {
            System.err.println(e.getMessage());
        }
    }


    @Override
    public void moveValidate() {

    }

    @Override
    public void addPiece(Chessman chessman, String coordination) throws Exception {
        var square = checkCoordinate(coordination);
        if (pieces.containsKey(square))
            throw new Exception(coordination + "This square is ocupied");
        pieces.put(square, chessman);
    }

    @Override
    public void addPieces(Map<String, Chessman> chessmanMap) throws Exception {
        for (String coordinate : chessmanMap.keySet())
            try {
                addPiece(chessmanMap.get(coordinate), coordinate);
            } catch (Exception e) {
                pieces = new HashMap<>();
                throw e;
            }
    }


    private Square checkCoordinate(String coordinate) throws Exception {
        if (coordinate.trim().length() > 2)
            throw new Exception("cordination format is not valid " + coordinate + ", coordination format should be [a..h][1..8]");
        var file = getFile(coordinate);
        var rank = getRrank(coordinate);
        if (!(file >= 0 && file < squareSideSize) || !(rank >= 0 && rank < squareSideSize))
            throw new Exception("This coordinate is not true " + coordinate);
        return new Square(getIntToBaseSide(file, rank, squareSideSize));
    }

    @Override
    public Chessman firstPieceOnPath(String fromCoordinate, String toCoordinate) {
        var moveType = getMoveType(fromCoordinate, toCoordinate);
        if (moveType == MoveType.KNIGHT || moveType == moveType.SINGLE_CROSS || moveType == MoveType.SINGLE_STRAIGHT) {
            return null;
        }

        var fromRankInt = getRrank(fromCoordinate);
        var fromFileInt = getFile(fromCoordinate);

        var toRankInt = getRrank(toCoordinate);
        var toFileInt = getFile(toCoordinate);

        int rankDirection = (toRankInt - fromRankInt) > 0 ? +1 : -1;
        int fileDirection = (toFileInt - fromFileInt) > 0 ? +1 : -1;

        if (moveType == MoveType.CROSS) {
            for (int i = 1; true; i++) {
                var rankOnPath = fromRankInt + (i * rankDirection);
                var fileOnPath = fromFileInt + (i * fileDirection);
                if (rankOnPath == toRankInt && fileOnPath == toFileInt) {
                    return null;
                }
                var pieceOnPath = pieces.get(new Square(getIntToBaseSide(fileOnPath, rankOnPath, squareSideSize)));
                if (pieceOnPath != null)
                    return pieceOnPath;
            }
        } else if (moveType == MoveType.STRAIGHT) {
            if (fromFileInt == toFileInt) {
                for (int i = 1; true; i++) {
                    var rankOnPath = fromRankInt + (i * rankDirection);
                    var pieceOnPath = pieces.get(new Square(getIntToBaseSide(fromFileInt, rankOnPath, squareSideSize)));
                    if (pieceOnPath != null)
                        return pieceOnPath;
                }
            } else if (fromRankInt == toRankInt) {
                for (int i = 1; true; i++) {
                    var fileOnPath = fromFileInt + (i * fileDirection);
                    var pieceOnPath = pieces.get(new Square(getIntToBaseSide(fileOnPath, fromRankInt, squareSideSize)));
                    if (pieceOnPath != null)
                        return pieceOnPath;
                }
            }
        }
        return null;
    }

    @Override
    public DirectionType getDirection(String fromCoordinate, String toCoordinate) {
        var fromRankInt = getRrank(fromCoordinate);
        var toRankInt = getRrank(toCoordinate);
        if (toRankInt > fromRankInt) {
            return DirectionType.FORWARD;
        } else {
            return DirectionType.BACKWARD;
        }

    }

    @Override
    public MoveType getMoveType(String fromCoordinate, String toCoordinate) {
        var fromRankInt = getRrank(fromCoordinate);
        var fromFileInt = getFile(fromCoordinate);
        var toRankInt = getRrank(toCoordinate);
        var toFileInt = getFile(toCoordinate);
        var rankChange = Math.abs(fromRankInt - toRankInt);
        var fileChange = Math.abs(fromFileInt - toFileInt);
        if (rankChange != 0 && fileChange != 0) {
            log.log(Level.DEBUG, "Is cross");
            if (rankChange == fileChange) {
                if (rankChange == 1) {
                    log.log(Level.DEBUG, "Single Cross");
                    return MoveType.SINGLE_CROSS;
                } else {
                    log.log(Level.DEBUG, "Multi Cross");
                    return MoveType.CROSS;
                }
            } else {
                if (Math.abs(fileChange - rankChange) == 1 && fileChange + rankChange == 3) {
                    log.log(Level.DEBUG, "Knight Move");
                    return MoveType.KNIGHT;
                } else {
                    log.log(Level.DEBUG, "Unknown Move");
                    return MoveType.UNKNOWN;
                }
            }
        } else {
            if (rankChange + fileChange == 1) {
                log.log(Level.DEBUG, "Single straight");
                return MoveType.SINGLE_STRAIGHT;
            } else {
                log.log(Level.DEBUG, "straight");
                return MoveType.STRAIGHT;
            }

        }

    }

    @Override
    public String toString() {
        StringBuilder stringBuilder = new StringBuilder("");
        pieces.keySet().forEach(square -> {
            stringBuilder.append(pieces.get(square).getShortName())
                    .append(getStrCoordinate(square.getCoordinate(), squareSideSize))
                    .append(pieces.get(square).getShortColor()).append(" ");
        });
        return stringBuilder.toString();
    }
}
