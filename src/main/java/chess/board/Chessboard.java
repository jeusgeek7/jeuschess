package chess.board;

import chess.board.move.DirectionType;
import chess.board.move.MoveType;
import chess.piece.Chessman;

import java.util.Map;

public interface Chessboard {

    void setSquareSideSize(int dimension);

    int getDimension();

    void moveValidate();

    void addPiece(Chessman chessman, String coordinate) throws Exception;

    void addPieces(Map<String, Chessman> stringChessmanMap) throws Exception;

    Chessman getPiece(String coordination) throws Exception;

    MoveType getMoveType(String from , String to);

    Chessman firstPieceOnPath(String fromCoordinate, String toCoordinate);

    DirectionType getDirection(String fromCoordinate, String toCoordinate);

    void move(String from, String to);


}
