package chess.board.move;

public enum DirectionType {
    FORWARD, BACKWARD;
}
