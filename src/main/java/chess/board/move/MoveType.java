package chess.board.move;

public enum MoveType {
    STRAIGHT,SINGLE_STRAIGHT,CROSS,SINGLE_CROSS,
    KNIGHT,UNKNOWN;
}
