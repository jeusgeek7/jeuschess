package chess.board;

import java.util.Objects;

public class Square {
    private int coordinate;

    public Square() {

    }

    public Square(int coordinate) {
        this.coordinate = coordinate;
    }

    public int getCoordinate() {
        return coordinate;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Square)) return false;
        Square square = (Square) o;
        return this.coordinate == square.coordinate;
    }

    @Override
    public int hashCode() {
        return Objects.hash(coordinate);
    }


}
