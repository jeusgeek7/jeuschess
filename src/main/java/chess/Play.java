package chess;

import chess.cli.*;
import chess.factory.Standard;
import chess.game.Chess;

import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

public class Play {
    static Map<String, Command> commands = new HashMap<>();

    public static void main(String[] args) {
        Play play = new Play();
        var chess = new Standard().setFactory();
        play.addCommands(chess);
        Scanner in = new Scanner(System.in);
        play.addCommands(chess);
        String str = "help";
        play.getCommand(str).runCommand(str);
        do {
            str = in.nextLine();
            var strCommand = str.toLowerCase().strip().split("\\s+");
            var command = play.getCommand(strCommand[0]);
            if (command != null) {
                command.runCommand(strCommand);
            }
        } while (!str.equalsIgnoreCase("finish"));
    }

    private void addCommands(Chess chess) {
        var move = new Move(chess);
        commands.put(move.commandStr(), move);
        var show = new Show(chess);
        commands.put(show.commandStr(), show);
        var start = new Start(chess);
        commands.put(start.commandStr(), start);
        var help = new Help(chess);
        commands.put(help.commandStr(), help);
        var time = new Time(chess);
        commands.put(time.commandStr, time);
    }

    private Command getCommand(String strCommand) {
        var command = commands.get(strCommand);
        return command;
    }
}
