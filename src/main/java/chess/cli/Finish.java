package chess.cli;

import chess.game.Chess;

public class Finish extends Command {

    public Finish(Chess chess) {
        super(chess);
        commandStr = "finish";
    }

    @Override
    public boolean execute(String... arguments) {
        try {
            print(chess.getTimeElapsed());
        } catch (Exception e) {
            System.err.println(e.getMessage());
            return false;
        }
        return true;
    }


}
