package chess.cli;

import chess.game.Chess;

import java.util.Scanner;

public abstract class Command {
    Chess chess;
    Scanner in = new Scanner(System.in);
    public String commandStr;

    public Command(Chess chess) {
        this.chess = chess;
    }

    public void print(String line) {
        System.out.println(line);
    }

    protected void newScan() {
        var player = chess.getTurnPlayer() != null ? chess.getTurnPlayer().name() : "";
        System.out.print(player + ">");
    }

    public String commandStr() {
        return commandStr;
    }

    public void runCommand(String... arguments) {
        execute(arguments);
        newScan();
    }

    public abstract boolean execute(String... arguments);

}
