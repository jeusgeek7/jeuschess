package chess.cli;

import chess.game.Chess;

public class Show extends Command {

    public Show(Chess chess) {
        super(chess);
        commandStr = "show";
    }

    @Override
    public boolean execute(String... args) {
        print(chess.showBoard());
        return true;
    }

}
