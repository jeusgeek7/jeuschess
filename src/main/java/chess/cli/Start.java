package chess.cli;

import chess.game.Chess;

public class Start extends Command {

    public Start(Chess chess) {
        super(chess);
        commandStr = "start";
    }

    @Override
    public boolean execute(String... arguments) {
        try {
            chess.start();
            print("started...");
        } catch (Exception e) {
            print(e.getMessage());
        }
        return true;
    }


}
