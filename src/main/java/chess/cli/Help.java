package chess.cli;

import chess.game.Chess;

public class Help extends Command {

    public Help(Chess chess) {
        super(chess);
        commandStr = "help";
    }

    @Override
    public boolean execute(String... arguments) {
        if (arguments.length == 1)
            System.out.println("[start]         starting game\n[finsih]        close game\n[move from to]  move piece" +
                    " from to\n[show]          show all piece in board\n[time]          show elapsed time");
        return true;
    }


}
