package chess.cli;

import chess.game.Chess;

public class Time extends Command {

    public Time(Chess chess) {
        super(chess);
        commandStr = "time";
    }

    @Override
    public boolean execute(String... arguments) {
        try {
            print(chess.getTimeElapsed());

        } catch (Exception e) {
            System.err.printf(e.getMessage());
            return false;
        }
        return true;
    }
}
