package chess.cli;

import chess.game.Chess;

public class Move extends Command {

    public Move(Chess chess) {
        super(chess);
        commandStr = "move";
    }

    @Override
    public boolean execute(String... args) {
        try {
            chess.move(args[1], args[2]);
        } catch (Exception e) {
            print(e.getMessage());
        }
        return true;
    }


}
