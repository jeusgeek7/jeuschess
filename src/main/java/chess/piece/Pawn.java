package chess.piece;

import chess.game.Player;

public class Pawn extends ChessmanAbstract implements Chessman {

    private static final Piece piece = Piece.PAWN;

    public Pawn(Player player) {
        super(player);
        super.piece = piece;
    }

}
