package chess.piece;

import chess.game.Player;


public abstract class ChessmanAbstract {
    protected Player player;
    protected Piece piece;

    public ChessmanAbstract(Player player) {
        this.player = player;
    }

    public String getName() {
        return piece.getName();
    }

    public char getShortName() {
        return piece.getShortName();
    }

    public char getFigure() {
        return player == Player.White ? piece.getWhiteFigure() : piece.getBlackFigure();
    }

    public char getShortColor() {
        return player.name().charAt(0);
    }

    public Piece getPiece() {
        return piece;
    }

    @Override
    public String toString() {
        return piece.getName() + " " + player.name();
    }


    public Player getPlayer() {
        return player;
    }


    public String getColor() {
        return player.name();
    }


}
