package chess.piece;

import chess.game.Player;

public class Knight extends ChessmanAbstract implements Chessman {
    private static final Piece piece = Piece.KNIGHT;

    public Knight(Player player) {
        super(player);
        super.piece = piece;
    }
}
