package chess.piece;

import chess.game.Player;

public class King extends ChessmanAbstract implements Chessman {
    private static final Piece piece = Piece.KING;

    public King(Player player) {
        super(player);
        super.piece = piece;

    }
}
