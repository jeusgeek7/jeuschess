package chess.piece;

public enum Piece {
    KING("King", 'K', '♔', '♚'),
    QUEEN("Queen", 'Q', '♕', '♛'),
    BISHOP("Bishop", 'B', '♗', '♝'),
    ROOK("Rook", 'R', '♖', '♜'),
    KNIGHT("Knight", 'K', '♘', '♞'),
    PAWN("Pawn", 'P', '♙', '♟');


    private String name;
    private char shortName;
    private char whiteFigure;
    private char blackFigure;

    Piece(String name, char shortName, char whiteFigure, char blackFigure) {
        this.name = name;
        this.shortName = shortName;
        this.blackFigure = blackFigure;
        this.whiteFigure = whiteFigure;
    }

    public String getName() {
        return name;
    }

    public char getShortName() {
        return shortName;
    }

    public char getWhiteFigure() {
        return whiteFigure;
    }

    public char getBlackFigure() {
        return blackFigure;
    }
}
