package chess.piece;

import chess.game.Player;

public class Queen extends ChessmanAbstract implements Chessman {
    private static final Piece piece = Piece.QUEEN;

    public Queen(Player player) {
        super(player);
        super.piece = piece;
    }
}
