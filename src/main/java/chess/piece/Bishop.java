package chess.piece;

import chess.game.Player;

public class Bishop extends ChessmanAbstract implements Chessman {
    private static final Piece piece = Piece.BISHOP;

    public Bishop(Player player) {
        super(player);
        super.piece = piece;
    }
}
