package chess.piece;

import chess.game.Player;

public interface Chessman {

    Player getPlayer();

    String getColor();

    String getName();

    char getShortName();

    char getShortColor();

    Piece getPiece();

}
