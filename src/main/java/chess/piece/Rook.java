package chess.piece;

import chess.game.Player;

public class Rook extends ChessmanAbstract implements Chessman {
    private static final Piece piece = Piece.ROOK;
    public Rook(Player player) {
        super(player);
        super.piece = piece;
    }
}
