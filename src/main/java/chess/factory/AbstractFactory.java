package chess.factory;

import chess.board.Chessboard;
import chess.board.ChessboardImpl;
import chess.game.Player;
import chess.game.Chess;
import chess.game.ChessImpl;
import chess.piece.*;

public abstract class AbstractFactory {

    protected String configuration;
    protected int side;
    protected String name;

    public Chess setFactory() {
        Chessboard chessboard = new ChessboardImpl();
        chessboard.setSquareSideSize(side);
        String[] configs = configuration.split("\\s+");
        for (String s : configs) {
            Chessman chessman = null;
            var coordinate = s.substring(1, 3);
            var ch = s.toUpperCase().charAt(0);
            var player = s.toLowerCase().charAt(3) == 'w' ? Player.White : Player.Black;
            switch (ch) {
                case 'P':
                    chessman = new Pawn(player);
                    break;
                case 'N':
                    chessman = new Knight(player);
                    break;
                case 'B':
                    chessman = new Bishop(player);
                    break;
                case 'Q':
                    chessman = new Queen(player);
                    break;
                case 'K':
                    chessman = new King(player);
                    break;
                case 'R':
                    chessman = new Rook(player);
                    break;
            }
            if (chessman != null) {
                try {
                    chessboard.addPiece(chessman, coordinate);
                } catch (Exception e) {
                    System.err.println(e.getMessage());
                }
            }

        }
        var chess = new ChessImpl();
        chess.setChessboard(chessboard);
        return chess;
    }

    public String getName(){
        return name;
    }
}
