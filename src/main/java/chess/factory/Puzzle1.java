package chess.factory;

public class Puzzle1 extends AbstractFactory implements Setup {

    private final String name = "Simple Puzzle";

    public Puzzle1() {
        configuration = "Ph1W Rb3B Pc3W Pd2W Ne3W Pf3W Kg3W Pb4B Pe4W Pe5W Pf5B Nb6B Pd6B Kf6B Pg6B Rg7W Rf8B";
        side = 8;
        super.name = name;
    }

    @Override
    public String getSoulotion() {
        return "1.d4 c5 2.d5 e5 3.c4 d6 4.Nc3 Nf6 5.e4 Be7 6.f3 O-O 7.Be3 Ne8 8.Qd2 a6 9.g4 Bh4+ 10.Bf2 Nd7 " +
                "11.Bxh4 Qxh4+ 12.Qf2 Qxf2+ 13.Kxf2 Rb8 14.Nge2 b5 15.cxb5 axb5 16.Ng3 b4 17.Nd1 g6 18.Ne3 Nb6 " +
                "19.h4 Bd7 20.Bd3 Nc7 21.h5 Nb5 22.Rh2 Nd4 23.Rah1 Kg7 24.hxg6 hxg6 25.Rh7+ Kf6 26.R7h6 Ra8 " +
                "27.Bb1 c4 28.Ngf5 Bxf5 29.gxf5 c3 30.fxg6 fxg6 31.Rh7 c2 32.Bxc2 Nxc2 33.Nxc2 Rxa2 34.Ne3 Rxb2+ " +
                "35.Kg3 Rb3";
    }
}
