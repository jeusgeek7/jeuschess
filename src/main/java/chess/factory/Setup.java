package chess.factory;


import chess.game.Chess;

public interface Setup {
    Chess setFactory();
    String getSoulotion();
    String getName();

}
