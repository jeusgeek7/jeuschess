package chess.factory;

public class Standard extends AbstractFactory implements Setup {
    private final String name = "Standar Chess";

    public Standard() {
        configuration =
                "Ra8B Nb8B Bc8B Qd8B Ke8B Bf8B Ng8B Rh8B " +
                "Pa7B Pb7B Pc7B Pd7B Pe7B Pf7B Pg7B Ph7B " +
                "Pa2W Pb2W Pc2W Pd2W Pe2W Pf2W Pg2W Ph2W " +
                "Ra1W Nb1W Bc1W Qd1W Ke1W Bf1W Ng1W Rh1W ";
        side = 8;
        super.name = name;
    }


    @Override
    public String getSoulotion() {
        return "Is Standard Playing";
    }
}
