package chess.game;

public enum Player {
    White("White"), Black("Black");
    String color;

    Player(String s) {
        color = s;
    }
}

