package chess.game;

import chess.board.Chessboard;


public interface Chess {

    void start() throws Exception;
    String turnColor();
    Player getTurnPlayer();
    void move(String from , String to) throws Exception;
    void setChessboard(Chessboard chessboard);
    String showBoard();
    String getTimeElapsed() throws Exception;

}
