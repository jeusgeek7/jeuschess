package chess.game;

import chess.board.Chessboard;
import chess.board.ChessboardImpl;
import chess.game.rule.*;
import chess.piece.*;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;


import java.time.Duration;
import java.time.Instant;

public class ChessImpl implements Chess {
    static Logger log = LogManager.getLogger();
    private Chessboard chessboard = new ChessboardImpl();
    private Player player;
    Instant startTime;
    // time passes


    @Override
    public void start() throws Exception {
        if (player != null)
            throw new Exception("The game has already started");
        player = Player.White;
        System.out.println(chessboard.toString());
        startTime = Instant.now();
    }

    @Override
    public String getTimeElapsed() throws Exception {
        if (startTime == null)
            throw new Exception("Game not started yet");
        Instant endTime = Instant.now();
        Duration timeElapsed = Duration.between(startTime, endTime);
        long seconds = timeElapsed.getSeconds();
        long absSeconds = Math.abs(seconds);
        String positive = String.format("%d:%02d:%02d", absSeconds / 3600, (absSeconds % 3600) / 60, absSeconds % 60);
        return seconds < 0 ? "-" + positive : positive;
    }

    @Override
    public String turnColor() {
        return player.name();
    }

    @Override
    public Player getTurnPlayer() {
        return player;
    }

    @Override
    public void move(String from, String to) throws Exception {

        Chessman chessman = chessboard.getPiece(from);
        if (chessman == null) {
            throw new Exception("[" + from + " TO " + to + "] not chessman at " + from);
        }
        if (chessman.getPlayer() != player) {
            throw new Exception("[" + from + " TO " + to + "] not your turn");
        }
        Chessman chessmanOnDestination = chessboard.getPiece(to);
        if (chessmanOnDestination != null && chessmanOnDestination.getPlayer() == player) {
            throw new Exception("[" + from + " TO " + to + "] your piece there, " + chessmanOnDestination.getName() +
                    " " + chessmanOnDestination.getColor().toLowerCase() + " at " + to);
        }
        var shortName = chessman.getShortName();
        MoveStrategy moveStrategy;
        switch (shortName) {
            case 'P':
                moveStrategy = new PawnMove();
                break;
            case 'N':
                moveStrategy = new KnightMove();
                break;
            case 'B':
                moveStrategy = new BishopMove();
                break;
            case 'Q':
                moveStrategy = new QueenMove();
                break;
            case 'K':
                moveStrategy = new KingMove();
                break;
            case 'R':
                moveStrategy = new RookMove();
                break;
            default:
                throw new IllegalStateException("Unexpected value: " + shortName);
        }
        var moveType = chessboard.getMoveType(from, to);
        var moveDirection = chessboard.getDirection(from, to);
        var onThePath = chessboard.firstPieceOnPath(from, to);
        moveStrategy.moveValidation(moveType, moveDirection, chessmanOnDestination, onThePath != null);
        System.out.println("MOVED---->");
        chessboard.move(from, to);
        nextTurn();
    }

    @Override
    public void setChessboard(Chessboard chessboard) {
        this.chessboard = chessboard;
    }

    public String showBoard() {
        return chessboard.toString();
    }

    private void nextTurn() {
        player = player == Player.White ? Player.Black : Player.White;
    }


}
