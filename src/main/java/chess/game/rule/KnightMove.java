package chess.game.rule;

import chess.board.move.DirectionType;
import chess.board.move.MoveType;
import chess.piece.Chessman;
import chess.piece.Piece;

public class KnightMove implements MoveStrategy {

    @Override
    public void moveValidation(MoveType moveType, DirectionType directionType, Chessman opponentOnDest, Boolean OnthePath) throws Exception {
        if (MoveType.STRAIGHT == moveType)
            throw new Exception("Knight can't move Stright");
        if (MoveType.SINGLE_STRAIGHT == moveType)
            throw new Exception("Knight can't move Stright");
        if (MoveType.CROSS == moveType)
            throw new Exception("Knight can't move cross");
        if (MoveType.SINGLE_CROSS == moveType)
            throw new Exception("Knight can't move cross");
        if (MoveType.UNKNOWN == moveType)
            throw new Exception("this unknown move");
        if (opponentOnDest != null)
            if (opponentOnDest.getPiece() == Piece.KING)
                throw new Exception("Knight can't X king");
    }
}
