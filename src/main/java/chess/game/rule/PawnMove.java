package chess.game.rule;

import chess.board.move.DirectionType;
import chess.board.move.MoveType;
import chess.piece.Chessman;
import chess.piece.Piece;

public class PawnMove implements MoveStrategy {


    @Override
    public void moveValidation(MoveType moveType, DirectionType directionType, Chessman opponentOnDest, Boolean OnthePath) throws Exception {
        if (moveType == MoveType.CROSS)
            throw new Exception("Pawn can move multi cross");
        if (moveType == MoveType.KNIGHT)
            throw new Exception("Pawn can move knight");
        if (moveType == MoveType.UNKNOWN)
            throw new Exception("this move unknown for pawn");
        if (moveType == MoveType.SINGLE_STRAIGHT && directionType == DirectionType.BACKWARD)
            throw new Exception("Pawn can't move backward");
        if (moveType == MoveType.STRAIGHT && directionType == DirectionType.FORWARD && opponentOnDest != null)
            throw new Exception("pawn can't X straight");
        if (moveType == MoveType.SINGLE_CROSS && opponentOnDest == null)
            throw new Exception("Pawn can't move cross if not have X");
        if (opponentOnDest != null)
            if (opponentOnDest.getPiece() != Piece.KING)
                throw new Exception("Pawn can't X king");
    }
}
