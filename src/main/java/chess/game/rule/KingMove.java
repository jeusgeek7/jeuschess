package chess.game.rule;

import chess.board.move.DirectionType;
import chess.board.move.MoveType;
import chess.piece.Chessman;
import chess.piece.Piece;

public class KingMove implements MoveStrategy {

    @Override
    public void moveValidation(MoveType moveType, DirectionType directionType, Chessman opponentOnDest,Boolean OnthePath) throws Exception {
        if (MoveType.STRAIGHT == moveType || MoveType.CROSS == moveType)
            throw new Exception("King Can't move more than one square");
        if (opponentOnDest != null)
        if (opponentOnDest.getPiece() == Piece.KING)
            throw new Exception("King can't X king");
    }
}
