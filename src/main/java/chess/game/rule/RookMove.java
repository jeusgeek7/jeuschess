package chess.game.rule;

import chess.board.move.DirectionType;
import chess.board.move.MoveType;
import chess.game.Player;
import chess.piece.Chessman;
import chess.piece.Piece;

public class RookMove implements MoveStrategy {

    @Override
    public void moveValidation(MoveType moveType, DirectionType directionType, Chessman opponentOnDest, Boolean OnthePath) throws Exception {
        if (moveType == MoveType.KNIGHT)
            throw new Exception("Pawn can move knight");
        if (moveType == MoveType.UNKNOWN)
            throw new Exception("this move unknown for pawn");
        if (moveType == MoveType.CROSS)
            throw new Exception("this move unknown for pawn");
        if (moveType == MoveType.SINGLE_CROSS)
            throw new Exception("this move unknown for pawn");
        if (OnthePath)
            throw new Exception("Some chessman is on the path");
        if (opponentOnDest != null)
            if (opponentOnDest.getPiece() == Piece.KING)
                throw new Exception("Rook can't X king");
    }
}

