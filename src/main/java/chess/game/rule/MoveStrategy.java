package chess.game.rule;

import chess.board.move.DirectionType;
import chess.board.move.MoveType;
import chess.piece.Chessman;

public interface MoveStrategy {
    void moveValidation(MoveType moveType, DirectionType directionType, Chessman OponentOnDest, Boolean OnthePath) throws Exception;
}
