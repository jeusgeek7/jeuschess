package chess.game.rule;

import chess.board.move.DirectionType;
import chess.board.move.MoveType;
import chess.game.rule.MoveStrategy;
import chess.piece.Chessman;
import chess.piece.Piece;

public class BishopMove implements MoveStrategy {


    @Override
    public void moveValidation(MoveType moveType, DirectionType directionType, Chessman opponentOnDest, Boolean OnthePath) throws Exception {
        if (MoveType.STRAIGHT == moveType)
            throw new Exception("Pawn can't move backward");
        if (MoveType.SINGLE_STRAIGHT == moveType)
            throw new Exception("pawn can't X straight");
        if (OnthePath)
            throw new Exception("Some chessman is on the path");
        if (opponentOnDest != null)
        if (opponentOnDest.getPiece() == Piece.KING)
            throw new Exception("Bishop can't X king");
    }
}
