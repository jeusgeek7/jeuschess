package chess.board;

import chess.board.move.MoveType;
import chess.game.Player;
import chess.piece.*;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import java.util.HashMap;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.*;

class ChessboardTest {
    private final String FromMove = "e5";
    private static final Map<String, String[]> moves = new HashMap<>();

    @BeforeAll
    static void addMoves() {
        System.out.println("-------------------------------");
        moves.put("SINGLE", "d4,d6,f4,f6,e4,d5,f5,e6".split(","));
        moves.put("MULTI", "a1,h2,b8,h8,e1,a5,h5,e8".split(","));
        moves.put("KNIGHT", "d3,f3,c4,g4,c6,g6,d7,f7".split(","));
        moves.put("UNKNOWN", "a2,d2,f2,b3,h3,d8,f8".split(","));
    }

    @Test
    void addPieceTest() {
        String[] coordinate = "a1,A8,b1,B8,g1,g8,h1,H8".split(",");
        ChessboardImpl chessboard = new ChessboardImpl();
        chessboard.setSquareSideSize(8);
        Chessman chessman = new Pawn(Player.White);
        for (String s : coordinate) {
            try {
                chessboard.addPiece(chessman, s);
            } catch (Exception e) {
                System.err.println(e.getMessage());
                fail();
            }
        }
    }

    @Test
    void addPieceExceptionTest() {
        String[] coordinate = "11,i1,I1,;7,08,a0,a9,a10,a-1".split(",");
        ChessboardImpl chessboard = new ChessboardImpl();
        chessboard.setSquareSideSize(8);
        Chessman chessman = new Pawn(Player.White);
        for (String s : coordinate) {
            try {
                chessboard.addPiece(chessman, s);
                fail(s);
            } catch (Exception e) {
                System.err.println(e.getMessage());
                assertTrue(true);
            }
        }
    }

    @Test
    void moveTest() {
        Square square = new Square();
        String[] coordinate = "a1,b2,c3,h7,h8,a9,i9".split(",");
        ChessboardImpl chessboard = new ChessboardImpl();
        chessboard.setSquareSideSize(8);
        Chessman chessman = new Pawn(Player.White);
        try {
            chessboard.addPiece(chessman, coordinate[0]);
        } catch (Exception e) {
            System.err.println(e.getMessage());
            fail();
        }
        for (int i = 0; i < coordinate.length - 1; i++) {
            try {
                chessboard.move(coordinate[i], coordinate[i + 1]);
                assertNotNull(chessboard.getPiece(coordinate[i + 1]), "Don't move to destination");
            } catch (Exception e) {
                System.err.println(e.getMessage());
            }
        }
    }

    @Test
    void getFile() {
        String[] coordinate = "a1,b2,c3,d7,e8,f9,g9,h8".split(",");
        int[] expectFile = {0, 1, 2, 3, 4, 5, 6, 7,};
        for (int i = 0; i < coordinate.length; i++) {
            int resultFile = ChessboardImpl.getFile(coordinate[i]);
            assertEquals(expectFile[i], resultFile, "file is not equal at index:" + i);
        }
    }

    @Test
    void getIntToBaseSideTest() {
        int[] expInt = {0, 8, 16, 24, 32, 40, 48, 56};
        for (int i = 0; i < 8; i++) {
            var resultInt = ChessboardImpl.getIntToBaseSide(0, i, 8);
            assertEquals(expInt[i], resultInt, "is not equal:" + 0 + i + " =>" + expInt[i]);
            var resultInt2 = ChessboardImpl.getIntToBaseSide(1, i, 8);
            assertEquals(expInt[i] + 1, resultInt2, "is not equal:" + 1 + i + " =>" + expInt[i] + 8);
        }
    }

    @Test
    void getRrank() {
        String[] coordinate = "a1,b2,c3,d4,e5,f6,g7,h8".split(",");
        int[] expectRank = {0, 1, 2, 3, 4, 5, 6, 7,};
        for (int i = 0; i < coordinate.length; i++) {
            int resultRank = ChessboardImpl.getRrank(coordinate[i]);
            assertEquals(expectRank[i], resultRank, "file is not equal at index:" + i);
        }
    }

    @Test
    void getStrCordinateTest() {
        int[] coordinateInt = {0, 1, 2, 3, 4, 5, 6, 7, 8};
        String[] expectStrCoord8 = "a1,b1,c1,d1,e1,f1,g1,h1,a2".split(",");
        String[] expectStrCoord7 = "a1,b1,c1,d1,e1,f1,g1,a2,b2".split(",");
        for (int i = 0; i < coordinateInt.length; i++) {
            String resultCoord = ChessboardImpl.getStrCoordinate(coordinateInt[i], 8);
            assertEquals(expectStrCoord8[i], resultCoord, "int to Str base 8 not equal index:" + i);
            String resultCoord2 = ChessboardImpl.getStrCoordinate(coordinateInt[i], 7);
            assertEquals(expectStrCoord7[i], resultCoord2, "int to Str base 7 not equal index:" + i);
        }
    }

    @Test
    void getIntCordinateTest() {
        String[] coordinateStr8 = "a1,a2,a3,a4,a5,a6,a7,a8,h1,h2,h3,h4,h5,h6,h7,h8".split(",");
        int[] expectIntCoord = {0, 8, 16, 24, 32, 40, 48, 56, 7, 15, 23, 31, 39, 47, 55, 63};
        for (int i = 0; i < coordinateStr8.length; i++) {
            int resultCoord = ChessboardImpl.getIntFromStrBaseSide(coordinateStr8[i], 8);
            assertEquals(expectIntCoord[i], resultCoord, "int to Str base 8 not equal index:" + i);
        }
    }

    @Test
    void getIntCordinate2Test() {
        var coordinateStr8 = "a1,b1,c1,d1,e1,f1,g1,h1,a8,b8,c8,d8,e8,f8,g8,h8".split(",");
        int[] expectIntCoord = {0, 1, 2, 3, 4, 5, 6, 7, 56, 57, 58, 59, 60, 61, 62, 63};
        for (int i = 0; i < coordinateStr8.length; i++) {
            int resultCoord = ChessboardImpl.getIntFromStrBaseSide(coordinateStr8[i], 8);
            assertEquals(expectIntCoord[i], resultCoord, "int to Str base 8 not equal index:" + i);
        }
    }

    @Test
    void getPiece() {
    }

    @Test
    void getMoveTypeSingle() {

        var chessboard = getChessBoardMove();
        MoveType singleStraight = MoveType.SINGLE_STRAIGHT;
        MoveType singleCross = MoveType.SINGLE_CROSS;
        String[] moveTo = moves.get("SINGLE");
        MoveType[] moveTypes = {singleCross, singleCross, singleCross, singleCross,
                singleStraight, singleStraight, singleStraight, singleStraight};
        for (int i = 0; i < moveTo.length; i++) {
            var moveType = chessboard.getMoveType(FromMove, moveTo[i]);
            assertEquals(moveTypes[i], moveType, "move type should be equal from:" + FromMove + " to:" + moveTo[i]);
        }
    }

    @Test
    void getMoveTypeMultiTest() {

        var chessboard = getChessBoardMove();
        MoveType straight = MoveType.STRAIGHT;
        MoveType cross = MoveType.CROSS;
        var moveTo = moves.get("MULTI");
        var moveTypes = new MoveType[]{cross, cross, cross, cross,
                straight, straight, straight, straight};
        for (int i = 0; i < moveTo.length; i++) {
            var moveType = chessboard.getMoveType(FromMove, moveTo[i]);
            assertEquals(moveTypes[i], moveType, "move type should be equal from:" + FromMove + " to:" + moveTo[i]);
        }

    }

    @Test
    void getMoveTypeKnighTest() {

        Chessboard chessboard = new ChessboardImpl();
        chessboard.setSquareSideSize(8);
        Chessman pawn = new Pawn(Player.White);
        String from = "e5";
        try {
            chessboard.addPiece(pawn, from);
        } catch (Exception e) {
            System.err.println(e.getMessage());
        }
        MoveType knight = MoveType.KNIGHT;
        var moveTo = moves.get("KNIGHT");
        var moveTypes = new MoveType[]{knight, knight, knight, knight, knight, knight, knight, knight};
        for (int i = 0; i < moveTo.length; i++) {
            var moveType = chessboard.getMoveType(from, moveTo[i]);
            assertEquals(moveTypes[i], moveType, "move type should be equal from:" + from + " to:" + moveTo[i]);
        }
    }

    @Test
    void getMoveTypeUnknownTest() {
        var chessboard = getChessBoardMove();
        MoveType unknown = MoveType.UNKNOWN;
        var moveTo = moves.get("UNKNOWN");
        var moveTypes = new MoveType[]{unknown, unknown, unknown, unknown, unknown, unknown, unknown};
        for (int i = 0; i < moveTo.length; i++) {
            var moveType = chessboard.getMoveType(FromMove, moveTo[i]);
            assertEquals(moveTypes[i], moveType, "move type should be equal from:" + FromMove + " to:" + moveTo[i]);
        }
    }

    @Test
    void firstPieceOnPathSingleMoveTest() {
        var chessboard = getChessBoardMove();
        var moveTo = moves.get("SINGLE");
        for (int i = 0; i < moveTo.length; i++) {
            var moveType = chessboard.firstPieceOnPath(FromMove, moveTo[i]);
            assertEquals(null, moveType, "chessman  should be equal in path from:" + FromMove + " to:" + moveTo[i]);
        }
    }

    @Test
    void firstPieceOnPathMultiMoveTest() {
        var chessboard = getChessBoardMove();
        var coordinates = "c3,g3,c7,g7,e3,b5,f5,e6".split(",");
        Map<String, Chessman> chessmans = new HashMap<>();
        chessmans.put(coordinates[0], new Pawn(Player.Black));
        chessmans.put(coordinates[1], new Bishop(Player.Black));
        chessmans.put(coordinates[2], new Pawn(Player.White));
        chessmans.put(coordinates[3], new Rook(Player.White));
        chessmans.put(coordinates[4], new Rook(Player.Black));
        chessmans.put(coordinates[5], new Knight(Player.Black));
        chessmans.put(coordinates[6], new Queen(Player.Black));
        chessmans.put(coordinates[7], new King(Player.Black));

        try {
            chessboard.addPieces(chessmans);
        } catch (Exception e) {
            fail("on create chessboard");
        }

        var moveTo = moves.get("MULTI");

        for (int i = 0; i < moveTo.length; i++) {
            var chessman = chessboard.firstPieceOnPath(FromMove, moveTo[i]);
            assertEquals(chessmans.get(coordinates[i]).getPlayer(), chessman.getPlayer(), "chessman  should be equal in path from:" + FromMove + " to:" + moveTo[i]);
            assertEquals(chessmans.get(coordinates[i]).getName(), chessman.getName(), "chessman  should be equal in path from:" + FromMove + " to:" + moveTo[i]);
        }
    }


    private Chessboard getChessBoardMove() {
        Chessboard chessboard = new ChessboardImpl();
        chessboard.setSquareSideSize(8);
        Chessman pawn = new Pawn(Player.White);
        String from = "e5";
        try {
            chessboard.addPiece(pawn, from);
        } catch (Exception e) {
            System.err.println(e.getMessage());
        }
        return chessboard;
    }
}