package chess.game;

import chess.factory.AbstractFactory;
import chess.factory.Standard;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ChessImplTest {

    @Test
    @Disabled("this test is disable right now")
    void turnColor() {
    }

    @Test
    @Disabled("this test is disable right now")
    void getTurnPlayer() {
    }

    @Test
    void moveFailTest() {
        String[] from = {"a2", "b2", "a2", "c2", "a2", "a2"};
        String[] to = {"b2", "a2", "a2", "d2", "d2", "e2"};
        var chess = new Standard().setFactory();
        try {
            chess.start();
        } catch (Exception e) {
            System.err.printf(e.getMessage());
        }
        for (int i = 0; i < from.length; i++) {
            try {
                chess.move(from[i], to[i]);
                fail("Pawn moved out of rule from:" + from[i] + " to:" + to[i]);
            } catch (Exception e) {
                System.err.println(e.getMessage());
                assertTrue(true);
            }
        }
    }

    @Test
    void moveTest() {
        String[] from = {"b2", "g7", "c2", "e7"};
        String[] to = {"b3", "g6", "c3", "e5"};
        var chess = new Standard().setFactory();
        try {
            chess.start();
        } catch (Exception e) {
            System.err.printf(e.getMessage());
        }
        for (int i = 0; i < from.length; i++) {
            try {
                chess.move(from[i], to[i]);
                assertTrue(true);
            } catch (Exception e) {
                fail("exception on moving from:" + from[i] + " to:" + to[i]);
            }
        }
    }
}