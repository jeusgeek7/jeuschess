
## Installation

## Glassory

[Chessboard](https://en.wikipedia.org/wiki/Chessboard): A chessboard is the type of gameboard used for the game of chess, on which the chess pawns and pieces are placed. A chessboard is usually square in shape, with an alternating pattern of squares in two colours

Square: Each square of the chessboard is identified by a unique coordinate pair—a letter and a number—from White's point of view.

[Files](https://en.wikipedia.org/wiki/Glossary_of_chess#file): The vertical columns of squares, called files, are labeled a through h from White's left (the queenside) to right (the kingside).

[Ranks](https://en.wikipedia.org/wiki/Glossary_of_chess#rank): The horizontal rows of squares, called ranks, are numbered 1 to 8 starting from White's side of the board.